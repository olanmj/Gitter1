﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Gitter1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Time = DateTime.Now;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "My git practice site.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Anonymous.";

            return View();
        }
    }
}